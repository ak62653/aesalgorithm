﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome to the Encrypt/Decrypt System");

        while (true)
        {
            Console.WriteLine("\nChoose an option:");
            Console.WriteLine("1. Encrypt");
            Console.WriteLine("2. Decrypt");
            Console.WriteLine("3. Exit");

            int option;
            if (!int.TryParse(Console.ReadLine(), out option))
            {
                Console.WriteLine("Invalid input. Please enter a valid option (1, 2, or 3).");
                continue;
            }

            switch (option)
            {
                case 1:
                    EncryptText();
                    break;
                case 2:
                    DecryptText();
                    break;
                case 3:
                    Console.WriteLine("Exiting the program.");
                    return;
                default:
                    Console.WriteLine("Invalid option. Please choose 1, 2, or 3.");
                    break;
            }
        }
    }

    static void EncryptText()
    {
        Console.WriteLine("\nEnter the text to be encrypted:");
        string plainText = Console.ReadLine();

        Console.WriteLine("Enter the secret key:");
        string key = Console.ReadLine();

        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = DeriveKey(key, aesAlg.KeySize / 8);
            aesAlg.GenerateIV();
            byte[] iv = aesAlg.IV;

            Console.WriteLine("Choose a block cipher mode:");
            Console.WriteLine("1. ECB");
            Console.WriteLine("2. CBC");
            Console.WriteLine("3. CFB");

            CipherMode mode = CipherMode.ECB;

            int modeOption;
            if (!int.TryParse(Console.ReadLine(), out modeOption) || modeOption < 1 || modeOption > 3)
            {
                Console.WriteLine("Invalid input. Defaulting to ECB.");
            }
            else
            {
                switch (modeOption)
                {
                    case 1:
                        mode = CipherMode.ECB;
                        break;
                    case 2:
                        mode = CipherMode.CBC;
                        break;
                    case 3:
                        mode = CipherMode.CFB;
                        break;
                }
            }

            string encryptedText = Encrypt(plainText, key, mode, iv);
            Console.WriteLine("\nEncrypted text:");
            Console.WriteLine(encryptedText);


            SaveToFile(encryptedText, iv);
        }
    }

    static void DecryptText()
    {
        Console.WriteLine("\nEnter the file path to read encrypted text:");
        string filePath = Console.ReadLine();
        string[] lines = File.ReadAllLines(filePath);

        byte[] iv = Convert.FromBase64String(lines[0]);
        string encryptedText = lines[1];

        Console.WriteLine("Enter the secret key:");
        string key = Console.ReadLine();

        Console.WriteLine("Choose a block cipher mode:");
        Console.WriteLine("1. ECB");
        Console.WriteLine("2. CBC");
        Console.WriteLine("3. CFB");

        CipherMode mode = CipherMode.ECB; // Default to ecb

        int modeOption;
        if (!int.TryParse(Console.ReadLine(), out modeOption) || modeOption < 1 || modeOption > 3)
        {
            Console.WriteLine("Invalid input. Defaulting to ECB.");
        }
        else
        {
            switch (modeOption)
            {
                case 1:
                    mode = CipherMode.ECB;
                    break;
                case 2:
                    mode = CipherMode.CBC;
                    break;
                case 3:
                    mode = CipherMode.CFB;
                    break;
            }
        }

        string decryptedText = Decrypt(encryptedText, key, mode, iv);
        Console.WriteLine("\nDecrypted text:");
        Console.WriteLine(decryptedText);
    }

    static string Encrypt(string plainText, string key, CipherMode mode, byte[] iv)
    {
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = DeriveKey(key, aesAlg.KeySize / 8);
            aesAlg.IV = iv;
            aesAlg.Mode = mode;

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            byte[] encryptedBytes;
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    byte[] plainBytes = Encoding.UTF8.GetBytes(plainText);
                    csEncrypt.Write(plainBytes, 0, plainBytes.Length);
                }
                encryptedBytes = msEncrypt.ToArray();
            }

            return Convert.ToBase64String(encryptedBytes);
        }
    }

    static string Decrypt(string cipherText, string key, CipherMode mode, byte[] iv)
    {
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = DeriveKey(key, aesAlg.KeySize / 8);
            aesAlg.IV = iv;
            aesAlg.Mode = mode;

            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            string decryptedText;
            using (MemoryStream msDecrypt = new MemoryStream(cipherBytes))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        decryptedText = srDecrypt.ReadToEnd();
                    }
                }
            }

            return decryptedText;
        }
    }

    static byte[] DeriveKey(string key, int keySize)
    {
        using (var sha256 = SHA256.Create())
        {
            byte[] hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(key));
            byte[] derivedKey = new byte[keySize];
            Array.Copy(hash, derivedKey, Math.Min(hash.Length, derivedKey.Length));
            return derivedKey;
        }
    }

    static void SaveToFile(string encryptedText, byte[] iv)
    {
        Console.WriteLine("\nEnter the file path to save encrypted text:");
        string filePath = Console.ReadLine();

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            writer.WriteLine(Convert.ToBase64String(iv));
            writer.WriteLine(encryptedText);
        }

        Console.WriteLine("Encrypted text and IV saved to file.");
    }
}





